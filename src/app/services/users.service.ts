import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap} from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { User } from '../models/user';


const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json', 'X-DreamFactory-API-Key': environment.API_KEY_LDAP}
  )};

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  public API_URL = environment.API_URL;

  constructor(private http: HttpClient) {}

  addUser (user: User): Observable<User> {
    return this.http.post<User>(this.API_URL+'user/session', user, httpOptions);
  }

}
