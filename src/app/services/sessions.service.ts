import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap} from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { User } from '../models/user';
import { CookieService } from 'ngx-cookie-service';
import {Query} from '../models/query';
import {LogSession} from '../models/log-session';
import {Resource} from '../models/resource';


@Injectable({
  providedIn: 'root'
})
export class SessionsService {
  public API_URL = environment.API_URL;

  constructor(
    private http: HttpClient,
    private cookie: CookieService) { }

  public httpOptions = {
    headers: new HttpHeaders({'X-DreamFactory-API-Key': environment.API_KEY_LDAP, 'Content-Type': 'application/json', 'X-DreamFactory-Session-Token': this.cookie.get('X-Token-Mind')}
    )};

  getData(){
    return this.http.get(this.API_URL+'user/session', this.httpOptions);
  }

  getLogSession(query: Query){
    return this.http.get<any>(this.API_URL+'LogUser/_table/loguser?filter='+query.filter, this.httpOptions);
  }

  updateStateLogSession(log_session: LogSession): Observable<LogSession> {
    return this.http.put<LogSession>(this.API_URL+'LogUser/_table/loguser/'+log_session._id, log_session, this.httpOptions);
  }

  insertLoginLog(resource: Resource): Observable<LogSession> {
    return this.http.post<LogSession>(this.API_URL+'LogUser/_table/loguser/', resource, this.httpOptions);
  }

  getInfoUsername(){
    return this.http.get<any>(this.API_URL+'Directorio_BRM/user/'+this.cookie.get('X-Username-Mind'), this.httpOptions);
  }

  deleteSesion(){
    return this.http.delete(this.API_URL+'user/session', this.httpOptions);
  }

}
