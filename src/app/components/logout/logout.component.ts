import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';
import {SessionsService} from '../../services/sessions.service';
import {LogSession} from '../../models/log-session';
import {Resource} from '../../models/resource';
import {CookieService} from 'ngx-cookie-service';
import {Query} from '../../models/query';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {
  public query: Query;
  public log_session: LogSession;
  public resource: Resource;
  public date = new Date();
  public data_resource = new Array();

  constructor(
    private router: Router,
    private session_service: SessionsService,
    private cookie: CookieService
  ) {
    this.query = new Query();
    this.log_session = new LogSession();
    this.resource = new Resource();
  }

  ngOnInit() {
    this.setStateSessions();
    this.router.navigate(['login']);
  }


  setStateSessions(){
    //Validate if exist session on the time, if session true pus state session of 0

    this.query.filter = "(username = '"+this.cookie.get('X-Username-Mind')+") and (session_state = 1)";
    this.session_service.getLogSession(this.query).subscribe(
      result => {
        console.log(result.resource.length);
        var i = 0;
        if (result.resource.length >= 1){
          for (let valor of result.resource){
            this.log_session = valor;
            this.log_session.date_logout = this.date.getFullYear()+"-"+this.date.getMonth()+"-"+this.date.getDate()+" "+this.date.toLocaleTimeString();
            this.log_session.session_state = 0;
            this.session_service.updateStateLogSession(this.log_session).subscribe(
              result => {
                console.log(result);
              }, error => {
                this.cookie.deleteAll();
                this.router.navigate(['login']);
              }
            );
            i = i + 1;
            if (i >= result.resource.length) {
              this.session_service.deleteSesion().subscribe(
                result => {
                  this.cookie.deleteAll();
                  this.router.navigate(['login']);
                  console.log(result);
                }, error => {
                  this.cookie.deleteAll();
                  this.router.navigate(['login']);
                  console.log(<any>error.error);
                }
              );
              this.cookie.deleteAll();
              break;
            }
          }
        }
      }, error => {
        this.cookie.deleteAll();
        console.log(<any>error.error);
      }
    );


  }

}
