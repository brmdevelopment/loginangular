import { Component, OnInit } from '@angular/core';
import { UsersService} from '../../services/users.service';
import { User } from '../../models/user';
import { Router} from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import {forEach} from '@angular/router/src/utils/collection';
import {SessionsService} from '../../services/sessions.service';
import {LogSession} from '../../models/log-session';
import {Resource} from '../../models/resource';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [UsersService]
})
export class LoginComponent implements OnInit {
  public user: User;
  public log_session: LogSession;
  public resource: Resource;
  public date = new Date();
  public data_resource = new Array();

  constructor(
    private user_service: UsersService,
    private session_service: SessionsService,
    private router: Router,
    private cookie: CookieService
  ) {
    this.user = new User();
    this.log_session = new LogSession();
    this.resource = new Resource();
  }

  ngOnInit() {
    this.session_service.getData().subscribe(
      result => {
        this.router.navigate(['home']);
      }, error => {
        this.cookie.deleteAll();
        console.log(<any>error.error);
      }
    );
  }

  onSubmit(){
    this.user.service="Directorio_BRM";
    this.user_service.addUser(this.user).subscribe(
      result => {
        this.cookie.set('X-Username-Mind', this.user.username);
        this.cookie.set('X-Token-Mind', result.session_token);
        this.saveLogin();
        this.router.navigate(['home']);
      }, error => {
        console.log(<any>error.error);
      }
    );
  }

  saveLogin(){
    this.log_session.username = this.user.username;
    this.log_session.session_state = 1;
    this.log_session.session_token = this.cookie.get('X-Token-Mind');
    this.log_session.date_login = this.date.getFullYear()+"-"+this.date.getMonth()+"-"+this.date.getDate()+" "+this.date.toLocaleTimeString();
    this.log_session.date_logout = " ";
    this.data_resource.push(this.log_session);
    this.resource.resource = this.data_resource;
    this.session_service.insertLoginLog(this.resource).subscribe(
      result => {
        console.log(result);
        location.reload();
      }, error => {
        console.log(<any>error.error);
      }
    );

  }

}
