import { Component, OnInit } from '@angular/core';
import { SessionsService } from '../../services/sessions.service';
import { Router} from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import {Query} from '../../models/query';
import {LogSession} from '../../models/log-session';



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public user: any;
  public token: string;
  public query: Query;
  public log_session: LogSession;
  public date = new Date();
  constructor(
    private session_service: SessionsService,
    private router: Router,
    private cookie: CookieService
  ) {
    this.query = new Query();
    this.log_session = new LogSession();

  }

  ngOnInit() {
    this.session_service.getData().subscribe(
      result => {
        this.user = result;
        if (!this.user.session_token){
          this.router.navigate(['login']);
        }else{
          this.getInfoUsername();
        }
      }, error => {
        this.router.navigate(['login']);
        console.log(<any>error.error);
      }
    );
  }

  getInfoUsername(){
    this.session_service.getInfoUsername().subscribe(
      result => {
        this.user = result;
        if (this.user.memberof) {
          if (Array.isArray(this.user.memberof)){
            for (let one_string of this.user.memberof){
              this.setCookie(one_string);
            }
          } else {
            this.setCookie(this.user.memberof);
          }
          this.redirect();
        } else {
          this.router.navigate(['home/logout']);
        }
      }, error => {
        //this.router.navigate(['home/logout']);
        console.log(<any>error.error);
      }
    );
  }

  private setCookie(param_memberof) {
    var memberof = param_memberof.split(',');
    var rol = null;
    var client = null;
    for (let element of memberof) {
      if (element.indexOf('CN=rol_') != -1) {
        rol = element.substring(element.indexOf('CN=rol_') + element.length, element.indexOf(','));
      }
      if (element.indexOf('CN=cliente_') != -1) {
        client = element.substring(element.indexOf('CN=cliente_') + element.length, element.indexOf(','));
      }
    }
    if (rol != null) {
      var rol_cookie = rol.split('CN=rol_');
      this.cookie.set('X-Rol-Mind', rol_cookie[1]);
    }
    if(client != null){
      var client_cookie = client.split('CN=cliente_');
      this.cookie.set('X-Client-Mind', client_cookie[1]);
    }
  }

  private redirect(){
    var rol = this.cookie.get('X-Rol-Mind');
    var client = this.cookie.get('X-Client-Mind');
    debugger;
    if(rol != null && client != null){
      if(rol == 'asesor' || rol == 'coordinador' ){
        location.href ="http://localhost:4201";
      }
    }else{
      this.router.navigate(['home/logout']);
    }
  }
}
