export class User {
  username: string;
  password: string;
  service: string;
  session_token: string;
}
