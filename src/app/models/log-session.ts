export class LogSession {
  _id: string;
  username: string;
  session_token: string;
  date_login: string;
  date_logout: string;
  session_state: number;
}
