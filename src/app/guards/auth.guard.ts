import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import  { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  public  token: string;
  constructor(
    private router: Router,
    private cookie: CookieService){

  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    this.token=this.cookie.get('X-Token-Mind');
    if (this.token){
      return true;
    }

    if (!this.token){
      this.cookie.deleteAll();
      const link = ['login'];
      this.router.navigate(link);
      return false;
    }

  }
}
